/**
 * Simple client with websocket
 * Author: Luis G. Leon Vega
 *
 * Copyright 2021 - Licensed under ISC
 */
const WebSocket = require('ws')

const client = new WebSocket('wss://localhost:8400')

client.on('message', msg => console.log('Message: ', msg))

client.once('open', () => {
  client.send('Hello')
})
