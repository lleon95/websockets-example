/**
 * Simple client with websocket
 * Author: Luis G. Leon Vega
 *
 * Copyright 2021 - Licensed under ISC
 */
const WebSocket = require('ws')

const client = new WebSocket('ws://localhost:8080')

client.on('message', msg => console.log('Message: ', msg))

client.once('open', () => {
  client.send('Hello')
})
