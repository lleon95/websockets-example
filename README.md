# Websockets Example

A simple websockets example

## Running simple client-server

```bash
# Terminal 1
cd server
node simple.js
# Terminal 2
cd client
node simple.js
```

## Running secure client-server

```bash
# Generate certs
openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 100 -nodes

# Terminal 1
cd server
node secure.js
# Terminal 2
cd client
NODE_TLS_REJECT_UNAUTHORIZED=0 node secure.js
```

The `NODE_TLS_REJECT_UNAUTHORIZED` environment variable allows you to use the
self-signed certificate for debugging purposes. It should not be placed in
production.
