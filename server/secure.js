/**
 * Simple echo server with websocket
 * Author: Luis G. Leon Vega
 *
 * Copyright 2021 - Licensed under ISC
 */

const port = 8400

const Fs = require('fs')
const Https = require('https')
const WebSocket = require('ws')

// Read the certificates
const privateKey = Fs.readFileSync('../key.pem', 'utf8')
const certificate = Fs.readFileSync('../cert.pem', 'utf8')
const credentials = { key: privateKey, cert: certificate }

// Create the HTTPS server
const httpsServer = Https.createServer(credentials)
httpsServer.listen(port)

const server = new WebSocket.Server({
  server: httpsServer
})

let sockets = []
server.on('connection', function (socket) {
  sockets.push(socket)
  console.log('Connected')
  console.log(socket)

  // When you receive a message, send that message to every socket.
  socket.on('message', function (msg) {
    sockets.forEach(s => s.send(msg))
    console.log('Message: ', msg)
  })

  // When a socket closes, or disconnects, remove it from the array.
  socket.on('close', function () {
    sockets = sockets.filter(s => s !== socket)
    console.log('Closed')
  })
})
